package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.service.AsignaturaService;

@Path("/asignaturas")
@RequestScoped
public class AsignaturaRESTService {

	@Inject
    private Logger log;

    @Inject
    AsignaturaService asignaturaService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Asignatura> listar() {
        return asignaturaService.seleccionar();
    }
    
    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Asignatura a) {

        Response.ResponseBuilder builder = null;

        try {
            asignaturaService.crear(a);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura creada exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @DELETE
    @Path("/nombre")
    public Response borrar(@PathParam("nombre") String nombre)
    {      
 	   Response.ResponseBuilder builder = null;
 	   try {
 		   
 		   if(asignaturaService.seleccionarPorNombre(nombre) == null) {
 			   
 			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
 		   }else {
 			   asignaturaService.borrar(nombre);
 			   builder = Response.status(202).entity("Asignatura borrada exitosamente.");			   
 		   }
 		   

 		   
 	   } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }
    
    
   @POST
   @Path("/{nombre}")
   public Response modificar(@PathParam("nombre") String nombre)
   {      
	   Response.ResponseBuilder builder = null;
	   try {
		   
		   if(asignaturaService.seleccionarPorNombre(nombre) == null) {
			   
			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
		   }else {
			   Asignatura a = new Asignatura();
			   a = asignaturaService.seleccionarPorNombre(nombre);
			   asignaturaService.modificar(a);
			   builder = Response.status(202).entity("Asignatura modificada exitosamente.");			   
		   }
		   

		   
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
   }
	
}