package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Matricula;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.dao.PersonaDAO;
import py.una.pol.personas.dao.AsignaturaDAO;

@Stateless
public class MatriculaDAO {

	@Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */
	 public String insertar(Matricula m) throws SQLException {

	        String SQL = "INSERT INTO matricula(cedula, nombre) "
	                + "VALUES(?,?)";
	 
	        String id = "";
	        Connection conn = null;
	        
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
	            pstmt.setString(1, m.getCedula());
	            pstmt.setString(2, m.getNombre());
	 
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getString(1);
	                    }
	                } catch (SQLException ex) {
	                	throw ex;
	                }
	            }
	        } catch (SQLException ex) {
	        	throw ex;
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
	        	
	        return id;
	    		
	    }
		
	 public String borrar(Matricula m) throws SQLException {

	        String SQL = "DELETE FROM matricula WHERE cedula = ? and nombre = ?";
	 
	        String id = "";
	        Connection conn = null;
	        
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	            pstmt.setString(1, m.getCedula());
	            pstmt.setString(2, m.getNombre());
	 
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getString(1);
	                    }
	                } catch (SQLException ex) {
	                	log.severe("Error en la eliminación: " + ex.getMessage());
	                	throw ex;
	                }
	            }
	        } catch (SQLException ex) {
	        	log.severe("Error en la eliminación: " + ex.getMessage());
	        	throw ex;
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        		throw ef;
	        	}
	        }
	        return id;
	    }
	
	public List<Persona> listarPorMateria(String nombre) 
{
		PersonaDAO personaDao = new PersonaDAO();
		String SQL = "SELECT cedula FROM matricula WHERE nombre = ? ";
		String iced = "";
		List<Persona> lista = new ArrayList<Persona>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1, nombre);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		
        		iced = rs.getString(1);
        		lista.add(personaDao.seleccionarPorCedula(iced));
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}
	
	public List<Asignatura> listarPorPersona(String cedula) 
{
		AsignaturaDAO asignaturaDao = new AsignaturaDAO();
		String SQL = "SELECT nombre FROM matricula WHERE cedula = ? ";
		String iname;
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setString(1, cedula);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		
        		iname = rs.getString(1);
        		lista.add(asignaturaDao.seleccionarPorNombre(iname));
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}
	
}
