package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.MatriculaDAO;
import py.una.pol.personas.dao.PersonaDAO;
import py.una.pol.personas.model.Matricula;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.Asignatura;

import java.util.List;
import java.util.logging.Logger;

@Stateless
public class MatriculaService {
	 @Inject
	    private Logger log;

	    @Inject
	    private MatriculaDAO dao;
	    
	    public void crear(Matricula m) throws Exception {
	        log.info("Creando Matricula: " + m.getCedula() + " " + m.getNombre());
	        try {
	        	dao.insertar(m);
	        }catch(Exception e) {
	        	log.severe("ERROR al crear persona: " + e.getLocalizedMessage() );
	        	throw e;
	        }
	        log.info("Matricula creada con éxito: " + m.getCedula() + " " + m.getNombre() );
	    }
	    
	    public String borrar(Matricula m) throws Exception {
	    	return dao.borrar(m);
	    }
	    
	    public List<Persona> seleccionarPorMateria(String name) {
	    	return dao.listarPorMateria(name);
	    }
	    
	    public List<Asignatura> seleccionarPorPersona(String cedula) {
	    	return dao.listarPorPersona(cedula);
	    }
	    
}
