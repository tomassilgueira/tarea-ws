package py.una.pol.personas.model;

import java.io.Serializable;

public class Matricula implements Serializable{
	String cedula;
	String nombre;
	
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
