package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless

public class AsignaturaService {

	@Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre() + " " + a.getDepartamento());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre() + " " + a.getDepartamento() );
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorNombre(String nombre) {
    	return dao.seleccionarPorNombre(nombre);
    }
    
    public String modificar(Asignatura a) throws SQLException {
    	return dao.actualizar(a);
    }
    
    public String borrar(String nombre) throws Exception {
    	return dao.borrar(nombre);
    }
	
}
