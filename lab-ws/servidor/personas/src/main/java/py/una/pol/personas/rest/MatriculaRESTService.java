package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Matricula;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.MatriculaService;

@Path("/matriculas")
@RequestScoped
public class MatriculaRESTService {
    @Inject
    private Logger log;

    @Inject
    MatriculaService matriculaService;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Matricula m) {

        Response.ResponseBuilder builder = null;

        try {
            matriculaService.crear(m);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("Matricula creada exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public Response borrar(Matricula m) {
    	 Response.ResponseBuilder builder = null;
	   try {
			   matriculaService.borrar(m);
			   builder = Response.status(202).entity("Matricula borrada exitosamente.");			   
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
    }

    @GET
    @Path("/persona")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Asignatura> obtenerPorIdQuery1(@QueryParam("cedula") String cedula) {
    	
    	return matriculaService.seleccionarPorPersona(cedula);
    }
    
    @GET
    @Path("/asignatura")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> obtenerPorIdQuery2(@QueryParam("nombre") String nombre) {
    	
    	return matriculaService.seleccionarPorMateria(nombre);
    }


}
