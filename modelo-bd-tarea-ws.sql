CREATE TABLE persona
(
    cedula character varying(1000),
    nombre character varying(1000),
    apellido character varying(1000),
    CONSTRAINT pk_cedula PRIMARY KEY (cedula)
)
WITH (
   OIDS=FALSE
 );


 
create table asignatura (
    nombre character varying(1000),
	departamento character varying(1000),
    semestre integer not null,
    CONSTRAINT pk_asignatura PRIMARY KEY (nombre)
);


create table matricula (
    cedula character varying(1000),
	nombre character varying(1000),
	CONSTRAINT pk_m PRIMARY KEY (cedula, nombre),
    CONSTRAINT fk_p FOREIGN KEY (cedula) REFERENCES persona(cedula),
	CONSTRAINT fk_a FOREIGN KEY (nombre) REFERENCES asignatura(nombre)
	
);

insert into persona values ('4260668', 'Jose', 'Silgueira');
insert into persona values ('1434035', 'Nidia', 'Trinidad');
insert into persona values ('1234567', 'Pablo', 'Marin');
insert into asignatura values ('fisica3', 'dcb', 5);
insert into asignatura values ('redes2', 'din', 5);
insert into asignatura values ('lp3', 'din', 5);
insert into matricula values('1434035','redes2');
insert into matricula values('1234567','fisica3');